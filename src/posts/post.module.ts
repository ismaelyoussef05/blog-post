import { Module,  } from "@nestjs/common";
import { Post, PostSchema } from "./schemas/post.schema";
import { MongooseModule } from "@nestjs/mongoose";
import { PostsController } from "./post.controller";
import { PoststService } from "./posts.service";




@Module({
  imports: [MongooseModule.forFeature(
    [
        {name: Post.name, schema: PostSchema}
    ]
  )],
  controllers: [PostsController],
  providers: [PoststService],
})
export class postsModule {}