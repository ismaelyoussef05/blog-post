import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
} from '@nestjs/common';
import { PoststService } from './posts.service';
import { CreatePostDto } from './dto/CreatePost.dto';
import { Post as userPost } from './schemas/post.schema';
import { UpdatePostDto } from './dto/UpdatePost.dto';

@Controller('posts')
export class PostsController {
  constructor(private postsService: PoststService) {}

  @Post()
  async createPost(@Body() CreatePostDto: CreatePostDto): Promise<userPost> {
    return this.postsService.createPost(CreatePostDto);
  }

  @Get()
  async getPost(): Promise<userPost[]> {
    return this.postsService.getPost();
  }

  @Get(':id')
  async getPostById(@Param('id') id: string): Promise<userPost> {
    return this.postsService.getPostById(id);
  }

  @Patch(':id')
  async updatePost(
    @Param('id') id: string,
    @Body() updatePostDto: UpdatePostDto,
  ): Promise<userPost> {
    return this.postsService.updatePost(id, updatePostDto);
  }

  @Delete(':id')
  async deletePostById(@Param('id') id: string): Promise<userPost> {
    return this.postsService.deletePostById(id);
  }
}
