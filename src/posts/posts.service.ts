import mongoose, { Model } from 'mongoose';
import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Post } from './schemas/post.schema';
import { CreatePostDto } from './dto/CreatePost.dto';
import { UpdatePostDto } from './dto/updatePost.dto';

@Injectable()
export class PoststService {
  constructor(@InjectModel(Post.name) private postshema: Model<Post>) {}

  async createPost(crtPostDto: CreatePostDto): Promise<Post> {
    const createdPost = await this.postshema.create(crtPostDto);
    return createdPost;
  }

  async getPost(): Promise<Post[]> {
    const allPost = await this.postshema.find();
    return allPost;
  }

  async getPostById(id: string): Promise<Post> {
    const isValid = mongoose.Types.ObjectId.isValid(id);
    if (!isValid) throw new BadRequestException('invalid Id');

    const findPost = await this.postshema.findById(id);
    if (!findPost) throw new NotFoundException('POST not found');

    return findPost;
  }

  async updatePost(id: string, updatePostDto: UpdatePostDto): Promise<Post> {
    const isValid = mongoose.Types.ObjectId.isValid(id);
    if (!isValid) throw new BadRequestException('invalid Id');

    const updatePost = await this.postshema.findByIdAndUpdate(
      id,
      updatePostDto,
      { new: true },
    );
    if (!updatePost) throw new NotFoundException('POST not found');

    return updatePost;
  }

  async deletePostById(id: string): Promise<Post> {
    const isValid = mongoose.Types.ObjectId.isValid(id);
    if (!isValid) throw new BadRequestException('invalid Id');

    const deletePost = await this.postshema.findByIdAndDelete(id);
    if (!deletePost) throw new NotFoundException('POST not found');

    return deletePost;
  }
}
